Overview
========

Pangolin is a set of tools for integrating various testing frameworks and continuous integration applications with TestRail.

It support a lot of integration scenarios like:  

*	Integration CI applications like like Jenkins, Bamboo, Teamcity and IBM UrbanCode with TestRail.
*   Integration various testing frameworks such as JUnit, testNG, VSTest, NUnit, Pytest, Python, Selenium, Ruby, etc with TestRail.