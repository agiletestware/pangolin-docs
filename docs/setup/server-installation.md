title: Server Installation Guide - Pangolin Documentation
description: How to install your Pangolin server

Server Installation
===================

----

## Requirements

* TestRail 5.3 or higher
* Windows OS/Linux OS
* For Linux only: Oracle JRE 1.8 or higher must be installed. Windows distributions contain built-in JRE.

---

## Windows

### Start Installation
[Download](https://www.agiletestware.com/pangolin#download) and run Pangolin Server installer

[![Pangolin installer](../img/setup/windows-installer-1.png)](../img/setup/windows-installer-1)

Accept the EULA

[![Pangolin eula](../img/setup/windows-installer-2.png)](../img/setup/windows-installer-2.png)

Specify the license file that was sent via email.

[![Pangolin license](../img/setup/windows-installer-3.png)](../img/setup/windows-installer-3.png)

Specify installation directory

[![Pangolin directory](../img/setup/windows-installer-4.png)](../img/setup/windows-installer-4.png)

Specify some available port 

[![Pangolin port](../img/setup/windows-installer-5.png)](../img/setup/windows-installer-5.png)

Installation progress

[![Pangolin progress](../img/setup/windows-installer-6.png)](../img/setup/windows-installer-6.png)

Finish screen shows Pangolin URL which takes you to the Pangolin administration page

[![Pangolin finish](../img/setup/windows-installer-7.png)](../img/setup/windows-installer-7.png)

## Linux
### Prerequisites

* 	Java Runtime Environment 1.8 must be installed and it's location must be added to PATH environment variable. To check if it's set run the following command in terminal `java -version`.

### Installation
The following steps need to be done in order to install and start Pangolin Server:

*	[Download Pangolin server archive](https://www.agiletestware.com/pangolin#download)
*	Unpack pangolin-unix-<version>.tar.gz into some directory, e.g. /opt
*	Copy your license file into pangolin directory
*	Run `sudo startup.sh` command from pangolin folder.

!!! tip	"Port specification"
	By default, Pangolin Server uses port 8080.  
	To change the port number, provide `-port <number>` command line option: e.g. `sudo startup.sh -port 9999`

## Pangolin Web Portal
The Pangolin URL takes you to the administration page, where you can view logs, view and update license, and view your encrypted password  


[![Pangolin web](../img/setup/windows-installer-8.png)](../img/setup/windows-installer-8.png)

----