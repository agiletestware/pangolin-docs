title: Pangolin Server Setting - Pangolin Documentation
description: Configuring Pangolin Server Security

Pangolin Server Settings
======================

Pangolin provides a settings page to set different configuration options for Pangolin Server.  
To access it, just navigate to the main page and click on "Settings" link.

[![Settings](../img/setup/settings-1.png)](../img/setup/settings-1.png)

If you are not logged in, it will redirect you on a login screen:

[![Login](../img/setup/login.png)](../img/setup/login.png)

*	Default user name: admin  
*	Password: admin

Please see [server security](server-security.md) for details

## Attachment Storage settings tab
Pangolin Server could upload test report attachments into TestRail directly or into various external systems and post URL into step's actual fields.  

By default, Pangolin uploads attachments directly into TestRail:

[![Attachment Settings](../img/setup/attachment-settings-0.png)](../img/setup/attachment-settings-0.png)

If you want to use external attachment storage, this storage must be configured first:

[![Attachment Settings](../img/setup/attachment-settings-1.png)](../img/setup/attachment-settings-1.png)

| Field Name  				 | Description                                                       																	|
|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| Type		 				 | Type of external storage: S3 - [Amazon S3](https://aws.amazon.com/s3/), Minio - [Minio](https://www.minio.io/) private cloud storage |                                                                                                                                                                                                                                                                                                                                          
| URL        				 | Base URL for external storage. For S3 - https://s3.amazonaws.com/ , for Minio - use your Minio server URL, like http://minio:9000	|
| API Key	 				 | API key																																|
| Secret Key 				 | Secret key																															|
| Folder path 				 | Path to a folder where attachments should be put into, e.g. bucketName/folder1/folder2												|
| Region	  				 | Region name (for S3 only). If it's not set, then the us-east-1 is used by default													|
| Disable upload attachments | Disable uploading of attachment into S3/Minio																						|

## TestRail settings tab
TestRail settings tab provides the following configuration capabilities:

### Project Fields
Since TestRail is highly customizable, users might need to adjust the default Pangolin settings to be able to export their test data correctly.    
This tab provides an ability to define different field names for different TestRail servers and projects.  

#### Adding a new configuration for TestRail project
To add a new configuration, click on a "Add new project" button:

[![Project Fields](../img/setup/project-fields-1.png)](../img/setup/project-fields-1.png)  

Fill the following fields in "Create New Project" dialog:  

*	**TestRail Server URL** - base URL for TestRail server, e.g. https://testrail.server.com
*	**TestRail Project Name** - name of a TestRail project to configure
*	**Cucumber Tags Fields** - name of custom Case Field into which Pangolin will export information about tags from Cucumber report. Leave it empy if you don't use Cucumber reports or don't want tags appear in TestRail
*	**Step Results Field** - name of custom Result Field into which test results should be stored. The type of this field in TestRail must be "Step Results". Default value is "custom_step_results"
*	**Test Case Template** - name of test case template which should be used for test cases. If it's not set, then "Test Case (Steps)" will be used.

 [![Project Fields](../img/setup/project-fields-2.png)](../img/setup/project-fields-2.png)
 
 After filling out all required fields, click on "Save button" and configuration will be saved.
 
 To update/delete configurations use "Edit" and "Delete" buttons in "Action" column of a configurations grid:
 
 [![Project Fields](../img/setup/project-fields-3.png)](../img/setup/project-fields-3.png)

## Proxy settings tab
Proxy settings tab allows users to setup proxy server parameters in case if there is a proxy server between Pangolin Server and TestRail:

[![Proxy Settings](../img/setup/proxy-1.png)](../img/setup/proxy-1.png)

To setup proxy settings, fill the following properties:  

*	**Proxy Host** - proxy protocol name and host name, e.g: http://proxyserver
*	**Proxy Port** - proxy port
*	**Proxy User** - user name, if proxy server requires authentication
*	**Proxy Password** - password, if proxy server requires authentication

and click on "Save" button.

To temporary disable proxy configuration - check "Disabled" checkbox.