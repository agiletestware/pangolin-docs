title: Pangolin Server Security - Pangolin Documentation
description: Configuring Pangolin Server Security

Configure Pangolin Server Security
======================

Pangolin provides a basic security for preventing unauthorized access to some configuration pages, so once user tries to access secured page it will be redirected to the login page:

[![Login](../img/setup/login.png)](../img/setup/login.png)

*	Default user name: admin  
*	Password: admin


## User management
To manage users, navigate to Settings -> Users:

[![User management](../img/setup/user-management-1.png)](../img/setup/user-management-1.png)
 
There are several buttons available:  

*	Add new user - adds new user
*	Delete - deletes a user
*	Change password - changes password. Available only for current user.

## Setting up secure connection to Pangolin Server
Here are the steps to setup a secure connection (HTTPS) to your Pangolin Server:

*	Generate or purchase an SSL/TLS certificate and generate a Java keystore of PKCS12 type (https://docs.oracle.com/javase/8/docs/technotes/tools/unix/keytool.html)
*	Put your keystore file (e.g. keystoreFile.pfx) into Pangolin Server installation directory (on Windows, it is usually C:\Program Files\pangolin)
*	Create a new ==application.yml== file in Pangolin Server installation directory
*	Add the following configuration into newly created ==application.yml==

```yml
server:
  port: 8443
  ssl:
    key-store: keystoreFile.pfx
    key-store-password: keystorePassword
    keyStoreType: PKCS12
```

, where `keystoreFile.pfx` is the name of your keystore file, `keystorePassword` password for your keystore

*	Windows: Restart `Pangolin TestRail Integration Service` 
*	Linux: Navigate to Pangolin Server installation directory and run `sudo ./shutdown.sh` and `sudo ./startup.sh` 