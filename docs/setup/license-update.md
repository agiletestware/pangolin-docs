title: Pangolin License Update - Pangolin Documentation
description: To update the license follow the steps in this guide

Updating license
===================

To update the license follow the steps below:

*	Open Pangolin main page in your browser (http://&lt;pangolin&#95;url:port&gt;) and click on "License Information" link or navigate directly to http://&lt;bumblebee&#95;server:port&gt;/pangolin/license

[![updating license](../img/setup/license-1.png)](../img/setup/license-1.png) 

*	Click on ==Browse== button, select new license file and click on ==Open== button
*	Click ==Upload== button

[![updating license](../img/setup/license-2.png)](../img/setup/license-2.png)