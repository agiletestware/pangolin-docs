title: Privacy Policy - Pangolin Documentation
description: Read Agiletestware Privacy Policy here

Agiletestware Privacy Policy
==========================


## 1. Introduction
This Privacy Policy is provided by: Agiletestware LLC  

Agiletestware adheres to applicable privacy laws and is committed to protecting your privacy and to provide a secure user experience. When you use Agiletestware Products, we may collect and process personal information about you. This Privacy Policy describes how we use your personal data and how you may protect your privacy.  

We will treat any personal information provided by you in accordance with this Privacy Policy, the End User License Agreement and applicable laws.

## 2. Personal data that we collect
When you use Agiletestware Products, we may collect a variety of information that may be linked to you as an individual, including:

*	information provided through your contact with us (such as name, email address, address, phone number);
*	information about our communication with you (such as customer support requests and feedback from you);
*	information that you send to our support team via product debugging logs (such as information application settings, errors and crashes, and settings regarding the use of installed Agiletestware Products);

We will not collect your contact information, such as your name, address and e-mail address, automatically, and will only process such information when you have provided it to us directly, e.g. through your contact with our customer services.  
Our software does not have any capabilities to transmit any information back to our servers.
 
## 3. How we use your personal data
Agiletestware uses your personal data to provide and improve Agiletestware Products which you use.  
In particular, we may use your personal data to:

*	manage your customer relationship with us;
communicate with you, including responding to your queries and feedback and sending you important notices such as errors and security updates to Agiletestware Products;
*	investigate, prevent or take action regarding illegal activities and violations of the End User License Agreement, other service specific terms, and applicable laws;
*	identify and reduce potential threats at an early stage; and
error recovery.

We may also use aggregated and anonymous information for any internal engineering improvements, in particular to improve the overall quality of Agiletestware Products.
 
## 4. How we may share your personal data

Agiletestware does not share your personal data with any firm without your consent.   
We do not sell your personal information to third parties.

## 5. How we secure your personal data
Agiletestware stores customer data in highly secure SAAS CRM systems (SalesForce). Email correspondence is also done via Google Gmail. In addition, we take active measure to train our employees on proper guidelines for securing their laptops, having strong passwords, and having anti-virus software on their local development systems.  

Notwithstanding such measures, Agiletestware cannot guarantee that unauthorised third parties will never be able to defeat those measures or use your information for improper purposes.

## 6. How can you contact us
You may contact our customer service by email: `contact@agiletestware.com` in order to receive information on personal data about you that we store and process.  
You may also request that we correct or delete your personal information.

## 7. Changes
We may update or otherwise amend our Privacy Policy from time to time at our own discretion, e.g. to reflect changes to the Agiletestware Products or changes to the law. The amendments will not have retroactive effect.  

We will update our product information to include any amendments to the Privacy Policy.  
The amendments will enter into force when communicated to you in an appropriate manner. Your continued use of Agiletestware Products after such publication or notification is considered as an acceptance of the amended Privacy Policy.  
If you do not agree to the amendments, you should terminate your use of Agiletestware Products.


