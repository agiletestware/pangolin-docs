title: Release Notes - Pangolin Documentation
description: Pangolin Release Notes, bugs and fixes


Release Notes
=============

**Pangolin version: 3.3**

New features:

*	Add support for new TestRail bulk API response
*	Add support of custom attachments for TestNG
*	Add support for testng_steps format: export test classes as tests and test methods as steps inside

**Pangolin version: 3.2**

New features:

*	Add new 'TestRail' attachment store which allows to upload attachments directly into TestRail
*	Cucumber: Add support for BeforeStep and AfterStep hooks. Attachments captured from these hooks are added to test result into TestRail 

Bugs Fixed:

*	CI Plugins: "Project must have an id" error when "configuration names" parameter is selected in CI plugin
*	Cucumber: Embedded attachments are ignored for Passed tests

**Pangolin version: 3.1**

New features:

*	Add 500 tests/day limitation for non-enterprise users

Bugs Fixed:

*	TestNG: Pangolin processes only the last 'test' element in the report

**Pangolin version: 3.0**

New features:

*	Add retry logic for TestRail Cloud API throttling.
*	JUnit: Add values of system-out and system-err elements into result in TestRail.
*	TestNG: Add report-output element contents into result in TR.
*	Jenkins plugin: Add post-build action to run report in TestRail.

Bugs Fixed:

*	Test titles larger than 250 characters causes error in TestRail.
*	Test is attached to a parent milestone if child milestone does not exist.
*	TestNG: Duration of test is not correctly exported into TestRail.

**Pangolin version: 2.9**

New features:

*	Add support for JUnit 5 format
*	CI Plugins: Add "Disable grouping" checkbox

Bugs Fixed:

*	Fix compatibility with OpenJDK 8
*	Test duration is not set for NUnit results
*	Add support for JDK 11 (Linux version)

**Pangolin version: 2.8**

Bugs Fixed:

*	Fix compatibility with TestRail 2.7

**Pangolin version: 2.7**

*	Fix security vulnerability in XML parsing

**Pangolin version: 2.6**

New Features:

*	Configure name of TestRail template to use for test cases
*	Add Proxy server configuration page
*	Java Annotations package and Jenkins plugin: Map test results into existing tests in TestRail
*	CI Plugins: Case Fields and Result Fields configuration properties now allow multiple values and commas
*	Jenkins: Upload test results into a particular TestRail configuration

Bugs Fixed:

*	Java Annotations: TestNG - skipped tests are not exported into TestRail
*	Jenkins plugin: Security fix

**Pangolin version: 2.5**

New Features:

*	CI Plugins: Add checkbox for competing run in TestRail
*	CI Plugins: Pass values for result fields in TestRail
*	GUI for user management

**Pangolin version: 2.4**

New Features:

*	Add support for uploading attachments to test results
*	Support of Cucumber reports
*	Support of Robot framework
*	Jenkins plugin: Add TestRail run URL to the dashboard
*	Configurable field name for TestRail steps

**Pangolin version: 2.3**

New Features:

*	Java Annotations package: Trailing slash in URL is ignored 

Bugs Fixed:

Pangolin Server:

*	Cannot work if TestRail URL contains some path after host name


**Pangolin version: 2.2**

New Features:

*	Secure Pangolin Server configuration
*	Add "View Log" page

Bugs Fixed:

Pangolin Server:

*	Incorrect java settings in startup script
*	Empty logs on Windows
*	NUnit results do not contain error message, only stack trace
*	TestNG results do not contain stack trace
*	Ignored tests are not added to run
*	Trailing slash in Pangolin/TestRail URLs leads to weird error

CI Plugins:

*	Jenkins: CSRF vulnerability and missing permission checks

**Pangolin version: 2.1**

New Features:
	
*	First public release     