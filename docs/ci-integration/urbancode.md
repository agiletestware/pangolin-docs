title: IBM UrbanCode and TestRail Integration Guide - Pangolin Documentation
description: Pangolin is a plugin for IBM UrbanCode that allows users to integrate any testing framework with TestRail without any code changes or custom tools

# IBM UrbanCode + TestRail Integration Guide

!!! success "Why you need Pangolin"
	In many organizations, [IBM UrbanCode Build](https://www.ibm.com/us-en/marketplace/urbancode-build) is used to build software, run unit tests, and run various kinds of testing frameworks. Example: Selenium, JUnit, Pytest, TestNG, Visual Studio Test, etc. These test metrics are very important and should be reflected in TestRail to show the true health of any project. Agiletestware Pangolin TestRail Connector plugin for IBM UrbanCode Build allows users to integrate any testing framework with [TestRail](http://www.gurock.com/testrail/) without making any code changes or writing custom tools.

## - Prerequisites
*	Pangolin Server must be installed and accessible from IBM UrbanCode Build host

## - Installation of plugin
*	Download plugin zip package from <https://agiletestware.com/pangolin#download> 
*	Navigate to System -> Server: -> Plug-ins

[![Pangolin Installation](../img/ci-integration/urbancode/install-1.png)](../img/ci-integration/urbancode/install-1.png)


*	Click on ==Browse== button and choose plugin zip file
*	Click on ==Load== button

[![Pangolin Installation](../img/ci-integration/urbancode/install-2.png)](../img/ci-integration/urbancode/install-2.png)

[![Pangolin Installation](../img/ci-integration/urbancode/install-3.png)](../img/ci-integration/urbancode/install-3.png)

## - Configuration of "Pangolin: Upload test results into TestRail" step
To upload test results into TestRail, a new "Pangolin: Upload test results into TestRail" build step has to be added:

[![Configuration](../img/ci-integration/urbancode/config-1.png)](../img/ci-integration/urbancode/config-1.png)

[![Configuration](../img/ci-integration/urbancode/config-2.png)](../img/ci-integration/urbancode/config-2.png)

| Field Name            | Required	| Description														|
|-----------------------|-----------|-------------------------------------------------------------------|
| Pangolin URL      	| Yes		| URL for Pangolin server. Example: http://some-server:8888			|                                                                                                                                                                                                                                                                                                                                          
| TestRail URL      	| Yes 		| URL for your TestRail instance. Example: https://testrail:8080	|
| TestRail User			| Yes 		| Name of user in TestRail											|
| TestRail Password		| Yes 		| Password for TestRail user										|
| Upload Timeout		| Yes 		| The number of minutes to wait for the Pangolin server to process the request. 0 means wait indefinitely |
| Project           	| Yes 		| The name of project in TestRail to which results should be exported			|
| TestPath				| Yes 		| Path to where Pangolin should test definitions, must contain suite name in the beginning (for single-suite project, suite name is always 'Master'), e.g. Master\\Section1\\Section2 |	
| Format            	| Yes 		| Format of test results generated during the build. Available values: junit, nunit, testng, trx	|                                                                                                                                                                                                                                                                    
| Results File Pattern  | Yes 		| Provide path to the report file(s) generated during the build. This is a comma separated list of test result directories. You can also use Ant style patterns such as ==**/surefire-reports/*.xml==	|
| Test Run      		| No  		| Name of test run in TestRail to which test results will be added |
| Test Plan      		| No  		| Name of test plan in TestRail to which test results will be added |
| Milestone Path      	| No  		| Path to a milestone in TestRail to which test run/plan will be added. E.g. Milestone1\\Milestone2 |
| Case Fields  			| No  		| Values for case fields in TestRail can be specified in this field. The format is [TestRail field system name]=[value 1] and each field name\\value pair should start with the new line. E.g.: custom\_user\_field1=foo |
| Result Fields  		|	No		| Values for result fields in TestRail can be specified in this field. The format is [TestRail field system name]=[value 1] and each field name\\value pair should start with the new line. E.g.: custom\_user\_field1=foo |
| Fail build if upload unsuccessful | N/A | If checked, the build will be marked as failed if for any reason the plugin was not able to upload the results. This could be due to Pangolin server issues, TestRail server issues, network issues, etc.	|
| Close Run				| N/A		| If checked, Pangolin will close the test run in TestRail and archive its tests and results |

!!! note "Make sure step is executed even if test phase fails"
	In order to upload results to TestRail in case when test failed, just set ==Pre-Condition Script== field to "Always". In such a case results will be uploaded regardless of a result of execute test step.

## - Execution
Once the job is configured, simply run the process containing the job. Here is IBM UrbanCode Build build execution output example:

[![Execution](../img/ci-integration/urbancode/upload-results-log-1.png)](../img/ci-integration/urbancode/upload-results-log-1.png)

[![Execution](../img/ci-integration/urbancode/upload-results-ci-1.png)](../img/ci-integration/urbancode/upload-results-ci-1.png)

And results in TestRail:

Test Cases:  

[![Execution Results](../img/ci-integration/urbancode/upload-results-1.png)](../img/ci-integration/urbancode/upload-results-1.png)

Test Plan and Test Run:

[![Execution Results](../img/ci-integration/urbancode/upload-results-2.png)](../img/ci-integration/urbancode/upload-results-2.png)

[![Execution Results](../img/ci-integration/urbancode/upload-results-3.png)](../img/ci-integration/urbancode/upload-results-3.png)

Created Milestones hierarchy:

[![Execution Results](../img/ci-integration/urbancode/upload-results-4.png)](../img/ci-integration/urbancode/upload-results-4.png)	
