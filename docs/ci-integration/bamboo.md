title: Atlassian Bamboo&reg; + TestRail Integration Guide - Pangolin Documentation
description: Integrating Atlassian Bamboo® and TestRail is very easy and requires users to install Pangolin TestRail Connector Add-on from Atlassian Bamboo® Marketplace and do a simple configuration.

# Atlassian Bamboo&reg; + TestRail Integration Guide  

!!! success "Why you need Bumblebee"
    In many organizations, [Atlassian Bamboo](https://www.atlassian.com/software/bamboo)&reg; is used to build software, run unit tests, and run various kinds of testing frameworks.  Example: Selenium, JUnit, Pytest, TestNG, Visual Studio Test, etc. These test metrics are very important and should be reflected in TestRail to show the true health of any project. Agiletestware Pangolin TestRail Connector plugin for Atlassian Bamboo&reg; allows users to integrate any testing framework with [TestRail](http://www.gurock.com/testrail/) without making any code changes or writing custom tools.

Integrating of [Atlassian Bamboo](https://www.atlassian.com/software/bamboo)&reg; and [TestRail](http://www.gurock.com/testrail/) is very easy and requires user to install Pangolin TestRail Connector Add-on from Atlassian Bamboo&reg; Marketplace and do a simple configuration

## - Prerequisites
*	Pangolin Server must be installed and accessible from Atlassian Bamboo&reg; host

## - Installation of add-on
*	Download current version of add-on from <https://agiletestware.com/pangolin#download>
*	Click on "Administration" -> "Add-ons"

[![Pangolin Installation](../img/ci-integration/bamboo/install-1.png)](../img/ci-integration/bamboo/install-1.png)

*	Click on "Upload add-on" -> "Browse" and select downloaded .jar file and click on "Upload" button

[![Pangolin Installation](../img/ci-integration/bamboo/install-2.png)](../img/ci-integration/bamboo/install-2.png)

*	Restart Atlassian Bamboo&reg; server

## - Add-on configuration
Add-on configuration consists of two parts:

### - Global settings configuration
Pangolin global configurations are accessible on "Administration" - "Pangolin" - "Global Settings" page:

[![Pangolin Global Configuration](../img/ci-integration/bamboo/global-settings-1.png)](../img/ci-integration/bamboo/global-settings-1.png)

[![Pangolin Global Configuration](../img/ci-integration/bamboo/global-settings-2.png)](../img/ci-integration/bamboo/global-settings-2.png)

| Field Name                    | Description                                                                                                                                                                                                                                                                                                                                                                                       |
|-------------------------------|-------------------------------------------------------------------|
| Pangolin URL                  | URL for Pangolin server. Example: http://some-server:8888			|                                                                                                                                                                                                                                                                                                                                          
| TestRail URL                  | URL for your TestRail instance. Example: https://testrail:8080	|
| TestRail User	Name			| Name of user in TestRail											|
| TestRail Password				| Password for TestRail user										|
| Upload Timeout				| The number of minutes to wait for the Pangolin server to process the request. 0 means wait indefinitely |

When all fields are set, please click on "Save" button - Pangolin will check availability of entered URLs and encrypt your password.

### - "Pangolin: Upload test results into TestRail" build task configuration
To upload test results into TestRail, a new "Pangolin: Upload test results into TestRail" task has to be added:

[![Configuration](../img/ci-integration/bamboo/config-1.png)](../img/ci-integration/bamboo/config-1.png)

[![Configuration](../img/ci-integration/bamboo/config-2.png)](../img/ci-integration/bamboo/config-2.png)

| Field Name                       | Required | Description                                                                                                                                                                                                                                                                                                                                                                                       |
|-----------------------------------|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| TestRail User 	   | No	| If specified, overrides TestRail user name defined in Global Configuration	|
| TestRail Password  | No	| If specified, overrides TestRail user password defined in Global Configuration|
| Project           | Yes   | The name of project in TestRail to which results should be exported			|
| TestPath				   | Yes | Path to where Pangolin should test definitions, must contain suite name in the beginning (for single-suite project, suite name is always 'Master'), e.g. Master\\Section1\\Section2 |
| Format             | Yes      | Format of test results generated during the build. Available values: junit, nunit, testng, trx	|                                                                                                                                                                                                                                                                    
| Results File Pattern   | Yes      | Provide path to the report file(s) generated during the build. This is a comma separated list of test result directories. You can also use Ant style patterns such as ==**/surefire-reports/*.xml==	|
| Test Run      | No       | Name of test run in TestRail to which test results will be added |
| Test Plan      | No       | Name of test plan in TestRail to which test results will be added |
| Milestone Path      | No       | Path to a milestone in TestRail to which test run/plan will be added. E.g. Milestone1\\Milestone2 |
| Case Fields  |	No	| Values for case fields in TestRail can be specified in this field. The format is [TestRail field system name]=[value 1] and each field name\\value pair should start with the new line. E.g.: custom\_user\_field1=foo |
| Result Fields  |	No	| Values for result fields in TestRail can be specified in this field. The format is [TestRail field system name]=[value 1] and each field name\\value pair should start with the new line. E.g.: custom\_user\_field1=foo |
| Fail build if upload unsuccessful | N/A      | If checked, the build will be marked as failed if for any reason the plugin was not able to upload the results. This could be due to Pangolin server issues, TestRail server issues, network issues, etc.	|
| Close Run				| N/A		| If checked, Pangolin will close the test run in TestRail and archive its tests and results |
| Disable Grouping		| N/A		| If checked, Pangolin will ignore structure in report files and upload all test cases into one Section, defined by the Test Path parameter. |

!!! note "Make sure task is added to Final Tasks"
	In order to upload results to TestRail in case when test failed, just add "Pangolin: Upload test results into TestRail" task to ==Final Tasks== section of Atlassian Bamboo&reg; job.

### - Execution
Once the job is configured, simply run the plan. Here is Bamboo plan execution output example:

[![Execution](../img/ci-integration/bamboo/upload-results-log-1.png)](../img/ci-integration/bamboo/upload-results-log-1.png)

[![Execution](../img/ci-integration/bamboo/upload-results-ci-1.png)](../img/ci-integration/bamboo/upload-results-ci-1.png)

And results in TestRail:

Test Cases:  

[![Execution Results](../img/ci-integration/bamboo/upload-results-1.png)](../img/ci-integration/bamboo/upload-results-1.png)

Test Plan and Test Run:

[![Execution Results](../img/ci-integration/bamboo/upload-results-2.png)](../img/ci-integration/bamboo/upload-results-2.png)

[![Execution Results](../img/ci-integration/bamboo/upload-results-3.png)](../img/ci-integration/bamboo/upload-results-3.png)

Created Milestones hierarchy:

[![Execution Results](../img/ci-integration/bamboo/upload-results-4.png)](../img/ci-integration/bamboo/upload-results-4.png)
