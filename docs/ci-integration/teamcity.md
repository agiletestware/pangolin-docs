title: JetBrains Teamcity and TestRail Integration Guide - Pangolin Documentation
description: How to integrate Teamcity and TestRail using Pangolin. Pangolin is a plugin for TeamCity which allows users to integrate Teamcity and TestRail without code changes or custom tools.

# JetBrains Teamcity and TestRail Integration Guide

!!! success "Why you need Bumblebee"
    In many organizations, [JetBrains Teamcity](https://www.jetbrains.com/teamcity/) is used to build software, run unit tests, and run various kinds of testing frameworks.  Example: Selenium, JUnit, Pytest, TestNG, Visual Studio Test, etc. These test metrics are very important and should be reflected in TestRail to show the true health of any project. Agiletestware Pangolin TestRail Connector plugin for TeamCity allows users to integrate any testing framework with [TestRail](http://www.gurock.com/testrail/) without making any code changes or writing custom tools.

Integrating of [TeamCity](https://www.jetbrains.com/teamcity/) and [TestRail](http://www.gurock.com/testrail/) is very easy and requires user to install Pangolin TestRail Connector plugin and do a simple configuration

## - Prerequisites
*	Pangolin Server must be installed and accessible from TeamCity host

## - Plugin installation
*	Download current version of plugin from <https://agiletestware.com/pangolin#download>
*	Click on "Administration" -> "Plugins List"

[![Pangolin Installation](../img/ci-integration/teamcity/install-1.png)](../img/ci-integration/teamcity/install-1.png)

*	Click on "Upload plugin zip" -> "Browse" and select downloaded zip file and click on "Save" button

[![Pangolin Installation](../img/ci-integration/teamcity/install-2.png)](../img/ci-integration/teamcity/install-2.png)

*	Restart TeamCity server

## - Plugin configuration
Plugin configuration consists of two parts:

### - Global settings configuration
Pangolin global configurations are accessible on "Administration" - "Pangolin" - "Global Settings" page:

[![Pangolin Global Configuration](../img/ci-integration/teamcity/global-settings-1.png)](../img/ci-integration/teamcity/global-settings-1.png)

[![Pangolin Global Configuration](../img/ci-integration/teamcity/global-settings-2.png)](../img/ci-integration/teamcity/global-settings-2.png)

| Field Name                    | Description                                                                                                                                                                                                                                                                                                                                                                                       |
|-------------------------------|-------------------------------------------------------------------|
| Pangolin URL                  | URL for Pangolin server. Example: http://some-server:8888			|                                                                                                                                                                                                                                                                                                                                          
| TestRail URL                  | URL for your TestRail instance. Example: https://testrail:8080	|
| TestRail User	Name			| Name of user in TestRail											|
| TestRail Password				| Password for TestRail user										|
| Upload Timeout				| The number of minutes to wait for the Pangolin server to process the request. 0 means wait indefinitely |

When all fields are set, please click on "Save" button - Pangolin will check availability of entered URLs and encrypt your password.

### - "Pangolin: Upload test results into TestRail" build step configuration
To upload test results into TestRail, a new "Pangolin: Upload test results into TestRail" build step has to be added:

[![Configuration](../img/ci-integration/teamcity/config-1.png)](../img/ci-integration/teamcity/config-1.png)

[![Configuration](../img/ci-integration/teamcity/config-2.png)](../img/ci-integration/teamcity/config-2.png)

| Field Name                       | Required | Description                                                                                                                                                                                                                                                                                                                                                                                       |
|-----------------------------------|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| TestRail User 	   | No	| If specified, overrides TestRail user name defined in Global Configuration	|
| TestRail Password  | No	| If specified, overrides TestRail user password defined in Global Configuration|
| Project           | Yes   | The name of project in TestRail to which results should be exported			|
| TestPath				   | Yes | Path to where Pangolin should test definitions, must contain suite name in the beginning (for single-suite project, suite name is always 'Master'), e.g. Master\\Section1\\Section2 |
| Format             | Yes      | Format of test results generated during the build. Available values: junit, nunit, testng, trx	|                                                                                                                                                                                                                                                                    
| Results File Pattern   | Yes      | Provide path to the report file(s) generated during the build. This is a comma separated list of test result directories. You can also use Ant style patterns such as ==**/surefire-reports/*.xml==	|
| Test Run      | No       | Name of test run in TestRail to which test results will be added |
| Test Plan      | No       | Name of test plan in TestRail to which test results will be added |
| Milestone Path      | No       | Path to a milestone in TestRail to which test run/plan will be added. E.g. Milestone1\\Milestone2 |
| Case Fields  |	No	|  Values for case fields in TestRail can be specified in this field. The format is [TestRail field system name]=[value 1] and each field name\\value pair should start with the new line. E.g.: custom\_user\_field1=foo |
| Result Fields  |	No	| Values for result fields in TestRail can be specified in this field. The format is [TestRail field system name]=[value 1] and each field name\\value pair should start with the new line. E.g.: custom\_user\_field1=bar |
| Fail build if upload unsuccessful | N/A      | If checked, the build will be marked as failed if for any reason the plugin was not able to upload the results. This could be due to Pangolin server issues, TestRail server issues, network issues, etc.	|
| Close Run				| N/A		| If checked, Pangolin will close the test run in TestRail and archive its tests and results |
| Disable Grouping		| N/A		| If checked, Pangolin will ignore structure in report files and upload all test cases into one Section, defined by the Test Path parameter. |

!!! note "Make sure step is executed even if test phase fails"
	In order to upload results to TestRail in case when test failed, just set ==Execute step== field to "Even if some of the previous steps failed". In such a case results will be uploaded regardless of a result of execute test step.

### - Execution
Once the job is configured, simply run the build. Here is TeamCity build execution output example:

[![Execution](../img/ci-integration/teamcity/upload-results-log-1.png)](../img/ci-integration/teamcity/upload-results-log-1.png)

[![Execution](../img/ci-integration/teamcity/upload-results-ci-1.png)](../img/ci-integration/teamcity/upload-results-ci-1.png)

And results in TestRail:

Test Cases:  

[![Execution Results](../img/ci-integration/teamcity/upload-results-1.png)](../img/ci-integration/teamcity/upload-results-1.png)

Test Plan and Test Run:

[![Execution Results](../img/ci-integration/teamcity/upload-results-2.png)](../img/ci-integration/teamcity/upload-results-2.png)

[![Execution Results](../img/ci-integration/teamcity/upload-results-3.png)](../img/ci-integration/teamcity/upload-results-3.png)

Created Milestones hierarchy:

[![Execution Results](../img/ci-integration/teamcity/upload-results-4.png)](../img/ci-integration/teamcity/upload-results-4.png)
