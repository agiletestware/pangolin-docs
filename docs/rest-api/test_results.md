title: Post test results - Pangolin Documentation
description: This API method provides the way for posting test results into TestRail.

# POST /pangolin/api/v1/test_results
## Description
This API method provides the way for uploading test results into TestRail

## Details
**Method:** POST

**Content-Type:** multipart/form-data

**Accept:** application/json 

### JSON structure
The method takes JSON document containing results as the body part with name **config**.
JSON document contains the following fields:

| Name              | Type   | Description													|
|-------------------|--------|--------------------------------------------------------------|
| url 	   			| String | TestRail server URL											|
| user				| String | User name for TestRail										|
| password			| String | Encrypted password for TestRail user. To encrypt password, navigate to http://pangolinUrl/pangolin/password |
| project			| String | The name of a TestRail project								|
| reportFormat		| String | Must always have value: **custom**							|
| testRun			| String | Optional. Name of a test run to which results must be assigned. If run with such name does not exist it will be created | 
| testPlan			| String | Optional. Name of a test plan to which results must be assigned. If plan with such name does not exist it will be created |
| milestonePath 	| String | Optional. Path to milestone to which results must be assigned. e.g. Milestone1\\Milestone2. If milestones do not exist, they will be created |
| runId				| Int	 | Optional. Id of a test run to which results must be assigned. |
| customFields		| String | Optional. Values for custom case fields in TestRail. The format is [TestRail field system name]=[value 1], [TestRail field system name]=[value 2]... E.g.: custom\_user\_field1=foo, custom\_user\_field2=bar |
| suites			| Array	 | An array of suites object (see below)						|

### Suites
==suites== array contains a real structure to be uploaded into TestRail:

| Name              | Type   | Description																				|
|-------------------|--------|------------------------------------------------------------------------------------------|
| name				| String | Name of a test suite in TestRail. Use **Master** if you have a single test suite project |
| sections			| Array	 | Array of section objects (see below)														|
| testRun			| String | Optional. Name of a test run to which results must be assigned. If run with such name does not exist it will be created | 
| testPlan			| String | Optional. Name of a test plan to which results must be assigned. If plan with such name does not exist it will be created |
| milestonePath 	| String | Optional. Path to milestone to which results must be assigned. e.g. Milestone1\\Milestone2. If milestones do not exist, they will be created |
| runId				| Int	 | Optional. Id of a test run to which results must be assigned. |

### Sections

==sections== array contains information about sections to be uploaded into TestRail:

| Name              | Type   | Description																				|
|-------------------|--------|------------------------------------------------------------------------------------------|
| name				| String | Name of a section in TestRail 															|
| sections			| Array	 | Array of child section objects															|
| cases				| Array	 | Array of child case objects (see below)													|

If section hierarchy does not exist in TestRail project, it will be created automatically.

### Cases
==cases== array contains information about test cases to be uploaded into TestRail:

| Name              | Type   | Description																				|
|-------------------|--------|------------------------------------------------------------------------------------------|
| name				| String | Name of a test case																		|
| status			| String | Execution status for test case: PASSED, FAILED, UNTESTED									|
| steps				| Array  | Array of test steps objects (see below)													|
| fields			| Array  | Array of custom case field name/value pairs, e.g. "custom\_case\_field" : "some value"	|

	
### Steps
==steps== array contains information about test steps

| Name              | Type   | Description																				|
|-------------------|--------|------------------------------------------------------------------------------------------|
| content			| String | Value for **content** field in TestRail													|
| expected			| String | Optional. Value for **expected** field in TestRail										|
| actual			| String | Optional. Value for **actual** field in TestRail											|
| status			| String | Execution status for test step: PASSED, FAILED, UNTESTED									|

## Examples

```json
{
  "url": "https://testrailUrl",
  "user": "user",
  "password": "cOv8VAZhdLwML3kESv+nNQxHp7rhEmHd",
  "project": "Pangolin Demo",
  "milestonePath": "Milestone1\\Milestone2",    
  "reportFormat": "custom",  
  "testPlan": "Test Plan",
  "testRun": "Test Run",
  "suites": [
    {      
      "name": "Master",      
	  "testPlan": "Overriding Test Plan",
      "testRun": "Overriding Test Run",
	  "milestonePath": "Milestone3\\Milestone4",
      "sections": [
        {
		  "name": "Section1",                   
          "sections": [
            {
				"name": "SubSection1",
				"cases": [
				{
				  "fields": {
					  "custom_required_field":"Something for required field",
					  "custom_optional_field":"Something for optional field"
				  },
				  "name": "Test Case 1",
				  "status": "PASSED",
				  "steps": [
					{
					  "content": "Step 1",
					  "actual": "Actual 1",					  
					  "expected": "Expected 1",
					  "status": "PASSED"
					},
					{
					  "content": "Step 2",
					  "actual": "Actual 2",					  
					  "expected": "Expected 2",
					  "status": "PASSED"
					}
				  ]
				},
				{
				  "fields": {
					  "custom_required_field":"Something for required field",
					  "custom_optional_field":"Something for optional field"
				  },
				  "name": "Test Case 2",
				  "status": "FAILED",
				  "steps": [
					{
					  "content": "Step 1",
					  "actual": "Actual 1",					  
					  "expected": "Expected 1",
					  "status": "FAILED"
					},
					{
					  "content": "Step 2",
					  "actual": "Actual 2",					  
					  "expected": "Expected 2",
					  "status": "PASSED"
					}
				  ]
				}
			  ]
			}
          ]
        }
      ]
    }
  ]
}

```

**Response example:**

```json
{
    "message": "OK",
    "properties": {},
    "runs": [
        {
            "id": 1705,
            "url": "https://testrailUrl/index.php?/runs/view/1705"
        }
    ]
}
```


**Results in TestRail:**
 
 [![Execution Results](../img/rest-api/results-1.png)](../img/rest-api/results-1.png)
 
 [![Execution Results](../img/rest-api/results-2.png)](../img/rest-api/results-2.png)
 
 [![Execution Results](../img/rest-api/results-3.png)](../img/rest-api/results-3.png)
 
 [![Execution Results](../img/rest-api/results-4.png)](../img/rest-api/results-4.png)
